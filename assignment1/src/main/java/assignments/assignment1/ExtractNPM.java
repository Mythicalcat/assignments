package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem

    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */

    public static boolean validate(long npm) {
        // TODO: validate NPM, return it with boolean

        // npm akan diubah menjadi string agar mudah diproses
        String npmString = String.valueOf(npm);
        int panjangNpm = npmString.length();


        // akan return false jika salah satu dari 4 kondisi tidak terpenuhi
        // jika panjang lenght kurang dari 14 berarti tidak valid
        if (panjangNpm != 14) return false;

        // jika kode jurusan tidak ada dalam variable KodeJurusanValid berarti tidak valid
        String kodeJurusanValid = "01|02|03|11|12";
        if (!kodeJurusanValid.contains(npmString.substring(2,4))) return false;

        // akan dicek tahun masuk pada npm, jika dibawah 15 berarti tidak valid
        if (!validTahun(npmString)) return false;

        // kode npm akan di cek kebenarannya jika tidak sesuai akan mereturn false
        long kodeNpmValid = cekKodeNPM(npmString.substring(0,panjangNpm-1));

        // jika kodenya lebih dari sepuluh akan dilakukan penjumlahan 2 digit tersebut
        while (kodeNpmValid >= 10){
            kodeNpmValid = jumSemua(kodeNpmValid);
        }

        // jika kodenya tidak sama dengan digit terakhir pada npm akan langsung direturn false
        if (kodeNpmValid != (npm%10)){
            return false;
        }

        return true;
    }
    // method untuk menambahkan semua digit dalam long

    public static long jumSemua(long npm){
        long jum = 0;

        while (npm > 0){
            // akan ditambah dengan digit pertama
            jum += npm % 10;
            // akan potong digit keduanya
            npm /= 10;
        }

        return jum;
    }

    // method untuk menngecek kevalidan tahun npm
    public static boolean validTahun(String npm){
        // membagi tahun masuk dan tahun lahir, tahun masuk +2000 karena 2 digit pertama adalah 20xx
        int tahunMasuk = Integer.parseInt(npm.substring(0,2))+2000;
        int tahunLahir = Integer.parseInt(npm.substring(8,12));


        // jika umur diatas 15 tahun berarti valid
        if (tahunMasuk - tahunLahir >= 15) return true;
        else return false;
    }

    // method untuk menngecek kevalidan kode npm
    public static long cekKodeNPM(String npm){


        // jika hanya ada 1 karakter tersisa pada string akan dikirim
        if (npm.length() == 1){
            return npm.charAt(0) - '0';
        }

        // akan diambil awal digit pertama dan terakhir pada string
        int awal = npm.charAt(0) - '0';
        int akhir = npm.charAt(npm.length()-1) - '0';

        // akan dilakukan pertambahan secara rekursi dari perkalian ujung ujung index
        return (((long) awal *akhir) + cekKodeNPM(npm.substring(1,npm.length()-1)));
    }

    // method untuk menngecek kevalidan kode npm jika isinya

    public static String extract(long npm) {
        // TODO: Extract information from NPM, return string with given format

        // npm akan diubah menjadi string agar mudah diproses
        String npmString = String.valueOf(npm);
        String hasilExtract = "";

        // tahun masuk akan diambil dari 2 digit pertama

        hasilExtract += "Tahun masuk: 20" + npmString.substring(0,2);

        // membuat switch case untuk jurusan
        String jurusan = switch (npmString.substring(2, 4)) {
            case "01" -> "Ilmu Komputer";
            case "02" -> "Sistem Informasi";
            case "03" -> "Teknologi Informasi";
            case "11" -> "Teknik Telekomunikasi";
            case "12" -> "Teknik Elektro";
            default -> " yang lain";
        };

        // menconcate  jurusan ke hasil akhir
        hasilExtract += "\nJurusan: " + jurusan;

        // menambahkan tanggal bulan tahun pada hasil
        String tanggal =npmString.substring(4, 6) ;
        String bulan   =npmString.substring(6, 8) ;
        String tahun   =npmString.substring(8, 12) ;

        hasilExtract += "\nTanggal Lahir: " + tanggal + "-" + bulan + "-" + tahun;



        return hasilExtract;
    }


    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            // TODO: Check validate and extract NPM

            // jika sudah tervalidasi akan diprint hasil extract NPM
            if(validate(npm)) System.out.println(extract(npm));

                // Jika tidak valid akan diprint
            else System.out.println("NPM tidak valid!\n");


        }
        input.close();
    }
}