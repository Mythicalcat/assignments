package assignments.assignment3;

class MataKuliah {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;

    private int jumlahMhs;

    public MataKuliah(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.kapasitas = kapasitas;
        daftarMahasiswa = new Mahasiswa[kapasitas];

    }

    public String getNama() {
        return nama;
    }

    public Dosen getDosen() {
        return dosen;
    }

    public boolean isPenuh(){
        return jumlahMhs == kapasitas;
    }

    private int searchMahasiswa(Mahasiswa mahasiswa){
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if(mahasiswa == daftarMahasiswa[i]) return i;
        }
        return -1;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        // validasi di kelas mahasiswa
        daftarMahasiswa[searchMahasiswa(null)] = mahasiswa;
        jumlahMhs++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        daftarMahasiswa[searchMahasiswa(mahasiswa)] = null;
        jumlahMhs--;
    }

    public void addDosen(Dosen dosen) {
        /* TODO: implementasikan kode Anda di sini */
        this.dosen = dosen;
    }

    public void dropDosen() {
        /* TODO: implementasikan kode Anda di sini */
        dosen = null;
    }

    public void printData(){
        // jika belum ada dosen, akan di print Belum ada
        String namaDosen = dosen != null ? dosen.getNama() : "Belum ada";

        System.out.println("Nama mata kuliah: " + nama +
                "\nJumlah mahasiswa: " + jumlahMhs +
                "\nKapasitas: " + kapasitas +
                "\nDosen pengajar: " + namaDosen +
                "\nDaftar mahasiswa yang mengambil mata kuliah ini:");

        int index = 0;

        for (Mahasiswa mhs:
             daftarMahasiswa) {
            if(mhs != null) {
                index++;
                System.out.println(index + ". " + mhs);
            }
        }

        // jika index masih 0 berarti daftar mahasiswa null semua
        if(index == 0){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        }
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return nama;
    }
}