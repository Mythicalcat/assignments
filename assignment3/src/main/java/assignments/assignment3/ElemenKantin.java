package assignments.assignment3;

import java.lang.management.ManagementFactory;

class ElemenKantin extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private Makanan[] daftarMakanan = new Makanan[10];
    private int jumlahMakanan;

    public ElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super(nama, "ElemenKantin");
    }

    public void setMakanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        if (jumlahMakanan == 10){
            System.out.println("jumlah makanan tidak boleh > 10");
        }

        if(getMakanan(nama) == null){
            // jika tidak ditemukan makanan dengan nama sama
            daftarMakanan[jumlahMakanan] = new Makanan(nama, harga);
            System.out.printf("%s telah mendaftarkan makanan %s dengan harga %d\n",this , nama, harga);
            jumlahMakanan++;
            return;
        }
        System.out.printf("[DITOLAK] %s sudah pernah terdaftar\n", nama);
    }

    public Makanan getMakanan(String nama){
        for (Makanan target:
             daftarMakanan) {
            // short-circuit evaluation, sehingga jika null tidak akan getnama
            if(target != null && target.getNama().equals(nama)) {
                return target;
            }
        }
        return null;
    }

}