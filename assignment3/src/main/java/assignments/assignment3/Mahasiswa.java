package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    public Mahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        super(nama, "Mahasiswa");
        this.npm = npm;
        this.tanggalLahir = extractTanggalLahir(npm);
        this.jurusan = extractJurusan(npm);
    }

    private String extractTanggalLahir(long npm) {
        /* TODO: implementasikan kode Anda di sini */
        String npmString = String.valueOf(npm);
        // dibuat menjadi integer kembali agak menghilangkan leading 0
        int tanggal =Integer.parseInt(npmString.substring(4, 6)) ;
        int bulan   =Integer.parseInt(npmString.substring(6, 8)) ;
        int tahun   =Integer.parseInt(npmString.substring(8, 12)) ;

        return (tanggal + "-" + (bulan) + "-" + tahun);
    }

    private String extractJurusan(long npm) {
        /* TODO: implementasikan kode Anda di sini */
        String kodeJurusan = String.valueOf(npm).substring(2, 4);

        return switch (kodeJurusan) {
            case "01" -> "Ilmu Komputer";
            case "02" -> "Sistem Informasi";
            default -> "Other";
        };
    }

    public int searchMatkul(MataKuliah mataKuliah){
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if(mataKuliah == daftarMataKuliah[i]) return i;
        }
        return -1;
    }

    public void addMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        // jika pernah diambil
        if(searchMatkul(mataKuliah) != -1){
            System.out.printf("[DITOLAK] %s telah diambil sebelumnya\n", mataKuliah.getNama());
            return;
        }
        // jika tidak penuh
        if(!mataKuliah.isPenuh()){
            daftarMataKuliah[searchMatkul(null)] = mataKuliah;
            mataKuliah.addMahasiswa(this);
            System.out.printf("%s berhasil menambahkan mata kuliah %s\n", this.getNama() ,mataKuliah.getNama());
            return;
        }
        System.out.printf("[DITOLAK] %s telah penuh kapasitasnya.\n", mataKuliah.getNama());
    }

    public void dropMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        int indexMatkul = searchMatkul(mataKuliah);

        // jika tidak ketemu
        if(indexMatkul == -1){
            System.out.printf("[DITOLAK] %s belum pernah diambil\n", mataKuliah.getNama());
            return;
        }

        System.out.println(this.getNama() +" berhasil drop mata kuliah DDP");
        daftarMataKuliah[indexMatkul] = null;
        mataKuliah.dropMahasiswa(this);
    }


    public void printData(){
        System.out.println("Nama: " + this.getNama() +
                "\nTanggal lahir: " + tanggalLahir+
                "\nJurusan: " + jurusan);


        // print matkul
        System.out.println("Daftar Mata Kuliah:");
        int index = 0;
        for (MataKuliah matkul:
             daftarMataKuliah) {
            if (matkul == null) continue;
            index++;
            System.out.println(index + ". " + matkul);
        }

        // kalau index masih 0 berati blm ada matkul
        if (index == 0){
            System.out.println("Belum ada mata kuliah yang diambil");
        }
    }
}