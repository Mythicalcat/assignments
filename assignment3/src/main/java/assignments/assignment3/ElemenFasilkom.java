package assignments.assignment3;

import java.security.PublicKey;

abstract class ElemenFasilkom implements Comparable<ElemenFasilkom> {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String tipe;

    private String nama;

    private int friendship;

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    // untuk dibandingkan dengan jumlah yg elemen
    int jumlahMenyapa;

    public ElemenFasilkom(String nama, String tipe){
        this.tipe = tipe;
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public int getFriendship() {
        return friendship;
    }

    private int cariElement(ElemenFasilkom cari){
        for (int i = 0; i < telahMenyapa.length ; i++) {
            if (telahMenyapa[i] == cari) return i;
        }
        return -1;
    }

    public boolean sudahMenyapa(ElemenFasilkom elemenFasilkom){
        return cariElement(elemenFasilkom) == -1;
    }

    public void menyapa(ElemenFasilkom elemenFasilkom) {
        /* TODO: implementasikan kode Anda di sini */
        telahMenyapa[cariElement(null)] = elemenFasilkom;

        // jumlah menyapa +1 untuk mempermudah perhitungan nilai friendhsip
        jumlahMenyapa++;

        // jika mahasiswa menyapa dosen
        // karena menyapanya 2 arah, jika dosen menyapa mahasiswa dipastikan mahahsiswa jg menyapa dosen
        if(this instanceof Mahasiswa) sapaDosen(elemenFasilkom);

    }

    private void sapaDosen(ElemenFasilkom elemenFasilkom){
        if(!(elemenFasilkom instanceof Dosen)) return;

        Dosen dosen = (Dosen) elemenFasilkom;

        // jika dosen tidak mengajar maka akan langsung return
        if(dosen.getMataKuliah() == null) return;

        Mahasiswa mhs = (Mahasiswa) this;

        // jika dosen mengajar matkul yang sama dengan matkul yang diambil, friendship +2
            if(mhs.searchMatkul(dosen.getMataKuliah()) != -1){
            friendship += 2;
            elemenFasilkom.friendship += 2;
        }
    }

    public void resetMenyapa() {
        /* TODO: implementasikan kode Anda di sini */
        // akan ditimpa oleh array baru dan di reset menyapanya
        telahMenyapa = new ElemenFasilkom[100];
        jumlahMenyapa = 0;
    }

    public void setFriendship(int totalElement){
        // jika menyapa < /2 total orang akan -5, jika lebih akan +10
        friendship += (jumlahMenyapa >= totalElement/2) ? 10 : -5;
        if(friendship < 0) friendship = 0;
        if(friendship > 100) friendship = 100;
    }


    // dibuat menjadi static karena pembeli dan penjual sudah ada di parameter
    public static void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        if(penjual instanceof ElemenKantin) {
            // downcasting
            ElemenKantin penjualKantin = (ElemenKantin) penjual;

            Makanan makanan  = penjualKantin.getMakanan(namaMakanan);

            if(makanan != null){
                System.out.printf("%s berhasil membeli %s seharga %d\n", pembeli.getNama(), namaMakanan, makanan.getHarga());
                // saat berhasil membeli makanan
                pembeli.friendship++;
                penjual.friendship++;
                return;
            }
        }

        System.out.printf("[DITOLAK] %s tidak menjual %s\n", penjual.getNama(), namaMakanan);
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return getNama();
    }

    // compare to untuk proses sorting
    public int compareTo(ElemenFasilkom o){
        // jika friendshipnya sama baru dibandingkan namanya
        if(friendship == o.getFriendship()){
            return this.getNama().compareTo(o.getNama());
        }

        // dibalik supaya descending
        return friendship < o.getFriendship()? 1 : -1;  
    }
}