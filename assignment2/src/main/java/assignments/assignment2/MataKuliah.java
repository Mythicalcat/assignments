package assignments.assignment2;

import java.security.PublicKey;
import java.util.Objects;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int jumlahMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        /* TODO: implementasikan kode Anda di sini */
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        // daftar mahasiswa akan sebanyak kapasitas
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }
    //setter getter

    public String getNama() {
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getSKS() {
        return this.sks;
    }

    public void setSks(int sks) {
        this.sks = sks;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public int getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(int kapasitas) {
        this.kapasitas = kapasitas;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return daftarMahasiswa;
    }

    public void setDaftarMahasiswa(Mahasiswa[] daftarMahasiswa) {
        this.daftarMahasiswa = daftarMahasiswa;
    }

    public int getJumlahMahasiswa() {
        return jumlahMahasiswa;
    }

    public void setJumlahMahasiswa(int jumlahMahasiswa) {
        this.jumlahMahasiswa = jumlahMahasiswa;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0; i < this.kapasitas; i++) {
            // akan diiterasikan sampai nememukan slot mahasiswa kosong kemudian dimasukan mahasiswa kesana
            if (Objects.isNull(daftarMahasiswa[i])){
                this.daftarMahasiswa[i] = mahasiswa;
                this.jumlahMahasiswa ++;
                break;
            }
        }


    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0; i < this.kapasitas; i++) {
            // akan diiterasikan sampai nememukan ahasiswa tersebut kemudian diisi ulang dengan null
            if (daftarMahasiswa[i] == mahasiswa){
                this.daftarMahasiswa[i] = null;
                this.jumlahMahasiswa --;
                break;
            }
        }

    }

    public void printNamaMahasiswa(){
        // jika ada mahasiswa nanti jadi false
        boolean tiadaMahasiwa = true;
        // untuk memprint jumlahnya
        int indexMahasiwa = 1;
        for (Mahasiswa peserta: this.daftarMahasiswa){
            if (!Objects.isNull(peserta)){
                System.out.println(indexMahasiwa +". "+ peserta.getNama());
                indexMahasiwa++;
                tiadaMahasiwa = false;
            }
        }
        if (tiadaMahasiwa){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        }
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        // akan direturn namanya
        return this.nama;
    }

    //method yang saya tambahkan
    public boolean equals(String nama){
        return this.nama.equals(nama);
    }

    public boolean equals(MataKuliah other){
        return this.nama.equals(other.getNama());
    }

    public boolean isAdaSlot (){
        for (Mahasiswa peserta:
                this.daftarMahasiswa) {
            if(Objects.isNull(peserta)){
                return true;
            }
        }
        return false;
    }
}
