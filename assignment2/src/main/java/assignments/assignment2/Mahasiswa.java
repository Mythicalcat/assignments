package assignments.assignment2;

import java.util.Arrays;
import java.util.Objects;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    //string jurusan saya pisah jadi 2 supaya mudah di cek kodenya
    private String[] jurusan;
    private long npm;
    //membuat total matkul untuk dipakai di beberapa method
    private int totalMatkul;
    // untuk dibandingkan nanti
    private final static int TIDAK_KETEMU = -1;

    public Mahasiswa(String nama, long npm){
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.npm = npm;
        // akan dimasukan pada jurusan yang ada berdasarkan npm
        this.jurusan = switch (masukanJurusan(npm)){
            case "01" -> new String[]{"Ilmu Komputer", "IK"};
            case "02" -> new String[]{"Sistem Informasi", "SI"};
            default -> new String[]{"kalo gaada ini ngambek intelijnya"};
        };
        //masalah IRS dipastikan kurang dari 20
        this.masalahIRS = new String[20];
    }
    // method tambahan untuk constructor
    private String masukanJurusan(long npm){
        //pemrosesan menggunakan string supaya mudah
        String strNPM = String.valueOf(npm);
        return strNPM.substring(2, 4);
    }


    // membuat setter getter field
    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public long getNpm() {
        return this.npm;
    }

    public void setNpm(long npm) {
        this.npm = npm;
    }

    public int getTotalSKS() {
        return this.totalSKS;
    }

    public void setTotalSKS(int totalSKS) {
        this.totalSKS = totalSKS;
    }

    public String getJurusan() {
        return this.jurusan[0];
    }

    public void setJurusan(String[] jurusan) {
        this.jurusan = jurusan;
    }

    public int getTotalMatkul() {
        return totalMatkul;
    }

    public void setTotalMatkul(int totalMatkul) {
        this.totalMatkul = totalMatkul;
    }

    public String[] getMasalahIRS() {
        return masalahIRS;
    }

    public void setMasalahIRS(String[] masalahIRS) {
        this.masalahIRS = masalahIRS;
    }

    public void addMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        if (isAddMatkulValid(mataKuliah)){
            // jika matkul bisa dimasukan, akan dimasukan pula ke matakuliah dan array matkul
            mataKuliah.addMahasiswa(this);
            this.totalMatkul++;
            this.totalSKS += mataKuliah.getSKS();

            int posisiMatkul = searchPosisiMatkul(null);
            this.mataKuliah[posisiMatkul] = mataKuliah;
        }
    }

    private boolean isAddMatkulValid(MataKuliah mataKuliah){
        boolean valid = false;

        // memulai proses validasi
        // akan di cek satu persatu sesuai dengan ketentuan ii - iii - i
        if (searchPosisiMatkul(mataKuliah) == Mahasiswa.TIDAK_KETEMU){
            if (mataKuliah.isAdaSlot()){
                if (!this.isMatkulMax()){
                    valid = true;
                }
                else {
                    System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
                }
            }
            else{
                System.out.printf("[DITOLAK] %s telah penuh kapasitasnya.\n", mataKuliah.getNama());
            }
        }
        else {
            System.out.printf("[DITOLAK] %s telah diambil sebelumnya.\n", mataKuliah.getNama());
        }
        return valid;
    }



    public void dropMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        // akan dicari index yang isinya null
        if (isDropMatkulValid(mataKuliah)){
            int posisiMatkul = searchPosisiMatkul(mataKuliah);

            // posisi yang sudah ada akan diberi null
            mataKuliah.dropMahasiswa(this);
            this.mataKuliah[posisiMatkul] = null;
            this.totalSKS -= mataKuliah.getSKS();
            this.totalMatkul--;
        }
    }

    private boolean isDropMatkulValid(MataKuliah mataKuliah){
        boolean valid = false;

        if (searchPosisiMatkul(mataKuliah) == Mahasiswa.TIDAK_KETEMU){
            System.out.printf("[DITOLAK] %s belum pernah diambil.\n", mataKuliah.getNama());
        }
        else {
            valid = true;
        }

        return valid;
    }

    //method tambahan juga ini
    public void printDaftarMatkul(){
        // membuat index untuk nanti ngeprint jadi urut
        int index = 1;

        if (this.totalMatkul != 0){
            for (MataKuliah matkul: this.mataKuliah) {
                if(!Objects.isNull(matkul)){
                    System.out.println((index)+". "+matkul);
                    index++;
                }
            }
        }
        else {
            System.out.println("Belum ada mata kuliah yang diambil");
        }
    }


    public void cekIRS(){
        /* TODO: implementasikan kode Anda di sini */
        // membuat string array baru untuk dimasukan, supaya tidak ada yg ganda
        this.masalahIRS = new String[20];

        // jika ada masalah akan berubah ke false
        boolean tiadaMasalah = true;
        int jumlahMasalah = 0;
        String masalah;

        if (this.totalSKS>24){
            // tiada masalah diubah menjadi false karena ada masalah
            tiadaMasalah = false;

            masalah = "SKS yang Anda ambil lebih dari 24";

            this.masalahIRS[jumlahMasalah] = masalah;
            jumlahMasalah++;
        }

        for (MataKuliah matkulCek : this.mataKuliah){
            // akan di cek apakah kode matkul
            if (!isKodeMatkulValid(matkulCek)){
                // tiada masalah diubah menjadi false karena ada masalah
                tiadaMasalah = false;

                // saya buat method baru karena kepanjanagan
                masalah = this.tipeMasalahMatkul(matkulCek.getNama(), this.jurusan[1]);
                this.masalahIRS[jumlahMasalah] = masalah;
                jumlahMasalah++;
            }
        }

        if (tiadaMasalah){
            // untuk handle jika tidak ada masalah jika di print dibawah ada '1.' nya
            System.out.println("IRS tidak bermasalah.");
        }
    }

    public void printDataIRS(){
        // akan kita cek dulu masalah IRSnya

        this.cekIRS();

        // masuk ke printing
        for (int i = 0; i < masalahIRS.length ; i++) {
            if(!Objects.isNull(this.masalahIRS[i])){
                System.out.println((i+1) +". "+ this.masalahIRS[i]);
            }
        }
    }

    private boolean isKodeMatkulValid(MataKuliah mataKuliah){
        // jika objek null akan diakan di skip
        if(Objects.isNull(mataKuliah)) return true;

        String kodeMatkul = mataKuliah.getKode();
        return (kodeMatkul.equals(this.jurusan[1]) || kodeMatkul.equals("CS") );
    }

    private String tipeMasalahMatkul(String mataKuliah, String kode){
        return "Mata Kuliah "+mataKuliah+" tidak dapat diambil jurusan "+kode;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        // akan direturn namanya
        return this.nama;
    }

    //tambahan dari saya
    public boolean equals(long npm){
        return this.npm == npm;
    }

    public boolean isMatkulMax(){
        // akan max jika sudah 10
        return this.totalMatkul >= 10;
    }

    public int searchPosisiMatkul(MataKuliah search){
        for (int i = 0; i < mataKuliah.length; i++) {
            // jika sesuai akan di return indexnya
            if (mataKuliah[i] == search){
                return i;
            }
        }

        // jika tidak ketemu akan di print -1
        return -1;
    }
}
