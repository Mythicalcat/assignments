package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HapusIRSGUI extends MainMenuGUI{

    private String[] npmList;
    private String[] matkulList;
    private JComboBox<String> opsiMhs;
    private JComboBox<String> opsiMatkul;

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Tambah IRS
        super(frame, daftarMahasiswa, daftarMataKuliah);
    }

    @Override
    protected void initGUI() {
        JPanel panel = createImagePanel();
        panel.setBackground(mainCol);
        panel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(5,5,5,5);

        // mengatur judul
        title.setText("Hapus IRS");
        title.setForeground(textCol);



        // membuat drop down menu

        opsiMhs = new JComboBox<>();
        opsiMhs.setPreferredSize(comboBoxDim);
        MainMenuGUI.titledBorderinit(opsiMhs, "Pilih NPM", textCol);

        opsiMatkul = new JComboBox<>();
        opsiMatkul.setPreferredSize(comboBoxDim);
        MainMenuGUI.titledBorderinit(opsiMatkul, "Pilih Nama Mata Kuliah", textCol);

        JButton hapusButton = new JButton("Hapus");
        hapusButton.setPreferredSize(buttonsDim);
        hapusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hapus();
            }
        });

        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setPreferredSize(buttonsDim);
        kembaliButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HomeGUI.setSwitcher("Home");
            }
        });


        // dekorasi
        stylize(opsiMatkul);
        stylize(opsiMhs);
        stylize(kembaliButton);
        stylize(hapusButton);

        // adding
        constraints.gridy++;
        panel.add(title);
        constraints.gridy++;
        panel.add(opsiMhs, constraints);
        constraints.gridy++;
        panel.add(opsiMatkul, constraints);
        constraints.gridy++;
        panel.add(hapusButton, constraints);
        constraints.gridy++;
        panel.add(kembaliButton, constraints);

        setPanel(panel);

    }

    // merefresh pilihan apa saja yang ada
    public void refreshList(){
        if(daftarMhs != null && daftarMhs.size() != 0){

            npmList = new String[daftarMhs.size()];
            // diambil npmnya
            for (int i = 0; i < daftarMhs.size(); i++) {
                npmList[i] = String.valueOf(daftarMhs.get(i).getNpm());
            }

            // disort
            Sorter.sortItem(npmList);

            // direfresh listnya
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(npmList);
            opsiMhs.setModel(model);
        }

        if(daftarMatkul != null && daftarMatkul.size() != 0){


            matkulList = new String[daftarMatkul.size()];
            // diambil namanya
            for (int i = 0; i < daftarMatkul.size(); i++) {
                matkulList[i] = daftarMatkul.get(i).getNama();
            }

            // disort
            Sorter.sortItem(matkulList);

            // direfresh listnya
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(matkulList);
            opsiMatkul.setModel(model);
        }
    }

    private void hapus(){
        if(opsiMatkul.getSelectedItem() == null  || opsiMhs.getSelectedItem() == null){
            // error tidak diisi filed
            JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");
            return;
        }

        String npm = (String) opsiMhs.getSelectedItem();
        Mahasiswa mahasiswa = getMahasiswa(Long.parseLong(npm));
        String nama = (String) opsiMatkul.getSelectedItem();
        MataKuliah mataKuliah = getMataKuliah(nama);

        // menambahkan matkul, constraint sudah di atur dalam add matkul
        JOptionPane.showMessageDialog(null, mahasiswa.dropMatkul(mataKuliah));
    }
}