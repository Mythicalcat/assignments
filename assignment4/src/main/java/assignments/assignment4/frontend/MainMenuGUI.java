package assignments.assignment4.frontend;

import assignments.assignment4.backend.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.event.*;
import java.util.ArrayList;

public abstract class MainMenuGUI {

    protected JLabel title;
    protected static Color mainCol = Color.decode("#CBD0A7");
    protected static Color textCol = Color.decode("#201936");
    protected Dimension textFieldDim = new Dimension(500, 50);
    protected Dimension comboBoxDim  = new Dimension(350, 50);
    protected Dimension buttonsDim   = new Dimension(200, 30);
    protected JPanel panel;
    protected JFrame frame;
    protected ArrayList<Mahasiswa> daftarMhs;
    protected ArrayList<MataKuliah> daftarMatkul;
    public static ImageIcon panelBg;
    public static ImageIcon busStop = new ImageIcon("assignment4/src/main/java/assignments/assignment4/assets/rain_bus.gif");
    public static ImageIcon imgWeeb = new ImageIcon("assignment4/src/main/java/assignments/assignment4/assets/rain_edit.gif");
    public static ImageIcon imgWeeb2= new ImageIcon("assignment4/src/main/java/assignments/assignment4/assets/wibu_edit.png");

    public MainMenuGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Tambah Mahasiswa
        this.frame = frame;
        this.daftarMatkul = daftarMataKuliah;
        this.daftarMhs = daftarMahasiswa;
        panel = createMainMenuTemplate();

        // disemua class ada titlenya
        title = new JLabel();
        title.setFont(SistemAkademikGUI.fontTitle);
        title.setForeground(textCol);

        // membuat guinya
        initGUI();
    }

    public JPanel getPanel() {
        return panel;
    }

    protected  <Comp extends Component> void setPanel(Comp component) {
        panel.add(component, BorderLayout.CENTER);
    }

    public static void titledBorderinit(JComponent panel, String title, Color color){
        Border tempBorder = BorderFactory.createLineBorder(color);
        TitledBorder buttonBorder = BorderFactory.createTitledBorder(tempBorder,title);
        buttonBorder.setTitleColor(color);
        panel.setBorder(buttonBorder);
    }

    // untuk membuat panel di class lain
    private JPanel createMainMenuTemplate(){
        JPanel temp = new JPanel();
        temp.setLayout(new BorderLayout());

        // memberikan border pada gambar
        JLabel borderImage = new JLabel();
        ImageIcon bdr = new ImageIcon("assignment4/src/main/java/assignments/assignment4/assets/border.png");
        borderImage.setIcon(bdr);
        temp.add(borderImage, BorderLayout.EAST);

        return temp;
    }

    protected JPanel createImagePanel(){
        panelBg = busStop;
        JPanel temp = new JPanel(){
            // membuat background
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(panelBg.getImage(), 0, 0, this);
            }
        };
        return temp;
    }

    // stylizer umum
    protected void stylize(Component component){
        component.setForeground(textCol);
        component.setBackground(mainCol);

        // supaya masih bisa dimasukan component lain
        if (component instanceof JButton){
            stylizeButton((JButton) component);
        }

        if (component instanceof JTextField){
            JTextField temp = (JTextField) component;
            temp.setOpaque(false);
        }

        if (component instanceof JComboBox){
            JComboBox temp = (JComboBox) component;
            temp.setOpaque(false);
            temp.setFocusable(false);
        }

        component.setFont(SistemAkademikGUI.fontGeneral);
    }

    private void stylizeButton(JButton button){
        button.setFocusable(false);
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                button.setBackground(HomeGUI.backColor);
                button.setForeground(HomeGUI.foreColor);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                button.setBackground(mainCol);
                button.setForeground(textCol);
            }
        });
    }

    // setiap class akan ada guinya sendiri
    protected abstract void initGUI();

    // getter untuk mengambil ditiap clas
    protected MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : daftarMatkul) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    protected Mahasiswa getMahasiswa(long npm) {

        for (Mahasiswa mahasiswa : daftarMhs) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }

}
