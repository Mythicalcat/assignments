package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI extends MainMenuGUI{
    private Mahasiswa mahasiswa;
    private Font consolasBold = new Font("Consolas", Font.BOLD , 12);

    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa,  ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        super(frame, daftarMahasiswa, daftarMataKuliah);
        this.mahasiswa = mahasiswa;
        // init gui setlah mahasiswa dimasukan
        initGUI();
    }

    @Override
    protected void initGUI() {
        if(mahasiswa == null){
            return;
        }

        JPanel panelUtama = new JPanel(){
            // membuat background
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(busStop.getImage(), 0, 0, this);
            }
        };
        panelUtama.setBackground(mainCol);
        panelUtama.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        // mengatur judul
        title.setText("Detail Ringkasan Mahasiswa");
        title.setForeground(textCol);

        // membuat detail
        JLabel isiBiodata = new JLabel();
        isiBiodata.setFont(consolasBold);
        isiBiodata.setForeground(textCol);
        isiBiodata.setText("<html><body>Nama: " + mahasiswa.getNama() + "<br>" +
                "NPM: " + mahasiswa.getNpm() + "<br>" +
                "Jurusan: " + mahasiswa.getJurusan() + "<br>" +
                "Daftar Mata Kuliah: " + mahasiswa.getJurusan() + "<br>" +"</body></html>");

        // memprint matkul
        JLabel matkulLable = new JLabel();
        matkulLable.setFont(consolasBold);
        matkulLable.setForeground(textCol);
        String isiMatkul = "<html><body>";
        int index = 0;
        for (MataKuliah matkul : mahasiswa.getMataKuliah()) {
            if (matkul== null) break;
            index++;
            isiMatkul += index +". " + matkul + "<br>";
        }

        isiMatkul += "</body></html>";

        // jka blm ambil
        if(index == 0){
            isiMatkul = "Belum ada mata kuliah yang diambil.";
        }

        matkulLable.setText(isiMatkul);

        JLabel SKS = new JLabel();
        SKS.setFont(consolasBold);
        SKS.setForeground(textCol);
        SKS.setText("<html><body>Total SKS: " + mahasiswa.getTotalSKS() + "<br>" +
                "Hasil Pengecekan IRS:" +"</body></html>");


        // cek IRS
        JLabel irsLable = new JLabel();
        irsLable.setFont(consolasBold);
        irsLable.setForeground(textCol);
        mahasiswa.cekIRS();

        // print IRS
        String hasilCek = "<html><body>";

        index = 0;
        for (String masalah: mahasiswa.getMasalahIRS()) {
            if (masalah == null) break;
            index++;
            hasilCek += index + ". " + masalah + "<br>";
        }

        hasilCek += "</body></html>";

        // jika tidak ada msalah
        if (index == 0){
            hasilCek = "IRS tidak Bermasalah";
        }

        irsLable.setText(hasilCek);

        //button kembali
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setPreferredSize(buttonsDim);
        stylize(kembaliButton);
        kembaliButton.addActionListener(e -> HomeGUI.setSwitcher("Home"));


        //menambahkan
        constraints.gridy = 1;
        panelUtama.add(title, constraints);
        constraints.gridy++;
        panelUtama.add(isiBiodata ,constraints);
        constraints.gridy++;
        panelUtama.add(matkulLable ,constraints);
        constraints.gridy++;
        panelUtama.add(SKS,constraints);
        constraints.gridy++;
        panelUtama.add(irsLable,constraints);
        constraints.gridy++;
        panelUtama.add(kembaliButton,constraints);

        panelUtama.setVisible(true);
        panel = panelUtama;
    }
}
