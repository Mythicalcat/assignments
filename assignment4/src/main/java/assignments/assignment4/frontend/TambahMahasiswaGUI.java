package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.util.Locale;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI extends MainMenuGUI{

    // biar bisa dibuka di method lain
    private JTextField entryNama;
    private JTextField entryNpm;

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Tambah Mahasiswa
        super(frame, daftarMahasiswa, daftarMataKuliah);
    }

    @Override
    protected void initGUI() {
        // mengatur panel bawaan
        JPanel tambahPanel = createImagePanel();
        tambahPanel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(5,5,5,5);

        // mengatur judul
        title.setText("Tambah Mahasiswa");
        title.setForeground(textCol);

        // membuat input
        JPanel panelEntry = new JPanel();
        panelEntry.setBackground(mainCol);
        panelEntry.setOpaque(false);
        panelEntry.setLayout(new GridBagLayout());

        entryNama = new JTextField();
        entryNama.setOpaque(false);
        entryNama.setPreferredSize(textFieldDim);
        MainMenuGUI.titledBorderinit(entryNama, "Nama", textCol);
        entryNama.setCaretColor(textCol);

        entryNpm = new JTextField();
        entryNpm.setOpaque(false);
        entryNpm.setPreferredSize(textFieldDim);
        MainMenuGUI.titledBorderinit(entryNpm, "NPM", textCol);
        entryNpm.setCaretColor(textCol);

        // membuat button
        JButton tambahButton = new JButton("Tambahkan");
        tambahButton.setPreferredSize(buttonsDim);
        tambahButton.addActionListener(e -> tambah());

        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setPreferredSize(buttonsDim);
        kembaliButton.addActionListener(e -> HomeGUI.setSwitcher("Home"));

        // menghias komponen
        stylize(entryNama);
        stylize(entryNpm);
        stylize(kembaliButton);
        stylize(tambahButton);


        // menambahkan ke panel
        constraints.gridy = 1;
        panelEntry.add(entryNama,constraints);
        constraints.gridy++;
        panelEntry.add(entryNpm,constraints);
        constraints.gridy++;
        panelEntry.add(tambahButton,constraints);
        constraints.gridy++;
        panelEntry.add(kembaliButton,constraints);


        // menambahkan ke panel utama
        constraints.gridy = 1;
        tambahPanel.add(title, constraints);
        constraints.gridy++;
        tambahPanel.add(panelEntry, constraints);
        tambahPanel.setVisible(true);
        setPanel(tambahPanel);
    }

    public void setKosong(){
        entryNama.setText("");
        entryNpm.setText("");
    }

    private void tambah(){
        String nama = entryNama.getText();

        cekEasterEgg(nama);

        // akan dimuncukan pesan error
        if(entryNpm.getText().equals("")  || nama.equals("")){
            // error tidak diisi filed
            JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");
            return;
        }

        // di cek dulu apakah text field npm kosong atau tidak
        long npm = Long.parseLong(entryNpm.getText());

        if(getMahasiswa(npm) != null){
            // error sudah pernah ditambah
            JOptionPane.showMessageDialog(null,String.format("NPM %d sudah pernah ditambahkan sebelumnya", npm));
            setKosong();
            return;
        }


        daftarMhs.add(new Mahasiswa(nama, npm));
        JOptionPane.showMessageDialog(null,String.format("Mahasiswa %d-%s berhasil ditambahkan", npm, nama));
        setKosong();
    }

    private void cekEasterEgg(String nama){
        if(nama.toLowerCase(Locale.ROOT).equals("gw wibu")){
            // easter egg ehehe
            panelBg = imgWeeb;
            JOptionPane.showMessageDialog(null,"mantap ketemu easter eggnya");
            return;
        }
        if(nama.toLowerCase(Locale.ROOT).equals("gw wibu 2")){
            // easter egg ehehe
            panelBg = imgWeeb2;
            JOptionPane.showMessageDialog(null,"mantap ketemu easter eggnya");
            return;
        }
        if(nama.toLowerCase(Locale.ROOT).equals("balikin woy") || nama.toLowerCase(Locale.ROOT).equals("balikin")){
            panelBg = busStop;
            JOptionPane.showMessageDialog(null,"siap");
            return;
        }
    }
}
