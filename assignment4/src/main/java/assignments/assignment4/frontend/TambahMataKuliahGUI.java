package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.jar.JarEntry;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI extends MainMenuGUI{

    private JTextField entryKode;
    private JTextField entryNama;
    private JTextField entrySKS;
    private JTextField entryKapasitas;

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // TODO: Implementasikan Tambah Mata Kuliah
        super(frame, daftarMahasiswa, daftarMataKuliah);
    }

    @Override
    protected void initGUI() {
        // mengatur panel bawaan
        JPanel tambahPanel = createImagePanel();
        tambahPanel.setBackground(mainCol);
        tambahPanel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(5,5,5,5);

        // mengatur judul
        title.setText("Tambah Mata Kuliah");
        title.setForeground(textCol);

        // membuat input
        JPanel panelEntry = new JPanel();
        panelEntry.setOpaque(false);
        panelEntry.setBackground(mainCol);
        panelEntry.setLayout(new GridBagLayout());

        entryKode = new JTextField();
        entryKode.setPreferredSize(textFieldDim);
        MainMenuGUI.titledBorderinit(entryKode, "Kode Mata Kuliah", textCol);
        entryKode.setCaretColor(textCol);

        entryNama = new JTextField();
        entryNama.setPreferredSize(textFieldDim);
        MainMenuGUI.titledBorderinit(entryNama, "Nama Mata Kuliah", textCol);
        entryNama.setCaretColor(textCol);

        entrySKS = new JTextField();
        entrySKS.setPreferredSize(textFieldDim);
        MainMenuGUI.titledBorderinit(entrySKS, "SKS", textCol);
        entrySKS.setCaretColor(textCol);

        entryKapasitas = new JTextField();
        entryKapasitas.setPreferredSize(textFieldDim);
        MainMenuGUI.titledBorderinit(entryKapasitas, "Kapasitas", textCol);;
        entryKapasitas.setCaretColor(textCol);

        // membuat button
        JButton tambahButton = new JButton("Tambahkan");
        tambahButton.setPreferredSize(buttonsDim);
        tambahButton.addActionListener(e -> tambah());

        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setPreferredSize(buttonsDim);
        kembaliButton.addActionListener(e -> HomeGUI.setSwitcher("Home"));

        // menghias komponen
        stylize(entryNama);
        stylize(entryKapasitas);
        stylize(entryKode);
        stylize(entrySKS);
        stylize(kembaliButton);
        stylize(tambahButton);


        // menambahkan ke panel
        constraints.gridy = 1;
        panelEntry.add(entryKode,constraints);
        constraints.gridy++;
        panelEntry.add(entryNama,constraints);
        constraints.gridy++;
        panelEntry.add(entrySKS,constraints);
        constraints.gridy++;
        panelEntry.add(entryKapasitas,constraints);
        constraints.gridy++;
        panelEntry.add(tambahButton,constraints);
        constraints.gridy++;
        panelEntry.add(kembaliButton,constraints);


        // menambahkan ke panel utama
        constraints.gridy = 1;
        tambahPanel.add(title, constraints);
        constraints.gridy++;
        tambahPanel.add(panelEntry, constraints);
        tambahPanel.setVisible(true);
        setPanel(tambahPanel);
    }


    private void tambah(){
        // akan dimuncukan pesan error
        if(isKosong()){
            // error tidak diisi filed
            JOptionPane.showMessageDialog(null,"Mohon isi seluruh Field");
            return;
        }

        // di cek dulu apakah text field npm kosong atau tidak
        String kode = entryKode.getText();
        String nama = entryNama.getText();
        int sks = Integer.parseInt(entrySKS.getText());
        int kapasitas = Integer.parseInt(entryKapasitas.getText());


        if(getMataKuliah(nama) != null){
            // error sudah pernah ditambah
            JOptionPane.showMessageDialog(null,String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", nama));
            setKosong();
            return;
        }


        daftarMatkul.add(new MataKuliah(kode, nama, sks, kapasitas));
        JOptionPane.showMessageDialog(null,String.format("Mata kuliah %s berhasil ditambahkan", nama));
        setKosong();
    }

    private boolean isKosong(){
        return (entryKapasitas.getText().equals("") ||
                entrySKS.getText().equals("") ||
                entryKode.getText().equals("") ||
                entryNama.getText().equals(""));
    }

    private void setKosong(){
        entryKode.setText("");
        entryNama.setText("");
        entrySKS.setText("");
        entryKapasitas.setText("");
    }
}
