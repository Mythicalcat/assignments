package assignments.assignment4.backend;

import java.util.*;

public class Sorter{

    // list sorting menggunakan quick sort dengan random pivot
    public static void sortItem(long[] arr){
        String[] arrString = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            arrString[i] = String.valueOf(arr[i]);
        }
        sort(arrString, 0, arr.length-1);
    }

    public static void sortItem(String[] arr){
        sort(arr, 0, arr.length-1);
    }

    static void swap(String[] arr, int i, int j)
    {
        String temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private static int partition(String[] arr, int low, int high)
    {
        // mengambil pivot paling kanan
        String pivot = arr[high];

        // mengmabil index paling bawah pada partisi supaya saat mengambil 1, pointer berada ditempat tepat
        int i = (low - 1);

        // memulai pengencekan dengan loop
        for(int j = low; j < high; j++)
        {
            if (arr[j].toLowerCase(Locale.ROOT).compareTo(pivot.toLowerCase(Locale.ROOT)) < 0)
            {
                // jika ada array pada posisi j yang < pivot, maka pointer akan dimajukan 1 kemudian di swap
                i++;
                swap(arr, i, j);
            }
        }
        // diswap pivot dan partisi
        swap(arr, i + 1, high);
        return (i + 1);
    }

    static void sort(String[] lis, int low, int high)
    {
        if (low < high)
        {


            int pi = partition(lis, low, high);

            // mensort ulang bagian yang low
            sort(lis, low, pi - 1);

            // mensort ulang bagian yang high
            sort(lis, pi + 1, high);
        }
    }

}
